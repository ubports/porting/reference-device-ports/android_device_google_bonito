#ifndef ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_SENSORMANAGER_H
#define ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_SENSORMANAGER_H

#include <android/frameworks/sensorservice/1.0/ISensorManager.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct SensorManager : public ISensorManager {
    // Methods from ::android::frameworks::sensorservice::V1_0::ISensorManager follow.
    Return<void> getSensorList(getSensorList_cb _hidl_cb) override;
    Return<void> getDefaultSensor(::android::hardware::sensors::V1_0::SensorType type, getDefaultSensor_cb _hidl_cb) override;
    Return<void> createAshmemDirectChannel(const hidl_memory& mem, uint64_t size, createAshmemDirectChannel_cb _hidl_cb) override;
    Return<void> createGrallocDirectChannel(const hardware::hidl_handle& buffer, uint64_t size, createGrallocDirectChannel_cb _hidl_cb) override;
    Return<void> createEventQueue(const sp<::android::frameworks::sensorservice::V1_0::IEventQueueCallback>& callback, createEventQueue_cb _hidl_cb) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" ISensorManager* HIDL_FETCH_ISensorManager(const char* name);

}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android

#endif  // ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_SENSORMANAGER_H
