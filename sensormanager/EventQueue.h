#ifndef ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUE_H
#define ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUE_H

#include <android/frameworks/sensorservice/1.0/IEventQueue.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct EventQueue : public IEventQueue {
    // Methods from ::android::frameworks::sensorservice::V1_0::IEventQueue follow.
    Return<::android::frameworks::sensorservice::V1_0::Result> enableSensor(int32_t sensorHandle, int32_t samplingPeriodUs, int64_t maxBatchReportLatencyUs) override;
    Return<::android::frameworks::sensorservice::V1_0::Result> disableSensor(int32_t sensorHandle) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" IEventQueue* HIDL_FETCH_IEventQueue(const char* name);

}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android

#endif  // ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUE_H
