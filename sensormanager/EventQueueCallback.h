#ifndef ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUECALLBACK_H
#define ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUECALLBACK_H

#include <android/frameworks/sensorservice/1.0/IEventQueueCallback.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct EventQueueCallback : public IEventQueueCallback {
    // Methods from ::android::frameworks::sensorservice::V1_0::IEventQueueCallback follow.
    Return<void> onEvent(const ::android::hardware::sensors::V1_0::Event& event) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
// extern "C" IEventQueueCallback* HIDL_FETCH_IEventQueueCallback(const char* name);

}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android

#endif  // ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_EVENTQUEUECALLBACK_H
