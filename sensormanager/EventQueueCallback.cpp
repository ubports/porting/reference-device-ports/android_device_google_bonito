#include "EventQueueCallback.h"

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

// Methods from ::android::frameworks::sensorservice::V1_0::IEventQueueCallback follow.
Return<void> EventQueueCallback::onEvent(const ::android::hardware::sensors::V1_0::Event& event) {
    // TODO implement
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IEventQueueCallback* HIDL_FETCH_IEventQueueCallback(const char* /* name */) {
    //return new EventQueueCallback();
//}
//
}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android
