#include <android-base/logging.h>
#include <hidl/HidlTransportSupport.h>
#include "SensorManager.h"

using ::android::OK;
using ::android::status_t;

using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;

using ::android::frameworks::sensorservice::V1_0::ISensorManager;
using ::android::frameworks::sensorservice::V1_0::implementation::SensorManager;

static int shutdown() {
    LOG(ERROR) << "SensorManager Service is shutting down.";
    return 1;
}

int main(int argc, char** argv)
{
    status_t status;
    android::sp<ISensorManager> service = nullptr;

    LOG(INFO) << "ISensorManager HAL Service Stub starting...";

    service = new SensorManager();
    if (service == nullptr) {
        LOG(ERROR) << "Error creating an instance of SensorManager.  Exiting...";
        return shutdown();
    }

    configureRpcThreadpool(1, true /* callerWillJoin */);

    status = service->registerAsService();
    if (status != OK) {
        LOG(ERROR) << "Could not register service for Netd HAL (" << status << ")";
        return shutdown();
    }

    LOG(INFO) << "SensorManager Service started successfully.";
    joinRpcThreadpool();
    // We should not get past the joinRpcThreadpool().
    return shutdown();
}
