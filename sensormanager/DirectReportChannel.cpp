#include "DirectReportChannel.h"

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

// Methods from ::android::frameworks::sensorservice::V1_0::IDirectReportChannel follow.
Return<void> DirectReportChannel::configure(int32_t sensorHandle, ::android::hardware::sensors::V1_0::RateLevel rate, configure_cb _hidl_cb) {
    // TODO implement
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IDirectReportChannel* HIDL_FETCH_IDirectReportChannel(const char* /* name */) {
    //return new DirectReportChannel();
//}
//
}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android
