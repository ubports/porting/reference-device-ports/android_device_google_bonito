#include "SensorManager.h"

#include "EventQueue.h"

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

// Methods from ::android::frameworks::sensorservice::V1_0::ISensorManager follow.
Return<void> SensorManager::getSensorList(getSensorList_cb _hidl_cb) {
    // TODO implement
    hidl_vec<hardware::sensors::V1_0::SensorInfo> ret;
    _hidl_cb(ret, Result::OK);
    return Void();
}

Return<void> SensorManager::getDefaultSensor(::android::hardware::sensors::V1_0::SensorType type, getDefaultSensor_cb _hidl_cb) {
    // TODO implement
    _hidl_cb({}, Result::OK);
    return Void();
}

Return<void> SensorManager::createAshmemDirectChannel(const hidl_memory& mem, uint64_t size, createAshmemDirectChannel_cb _hidl_cb) {
    // TODO implement
    return Void();
}

Return<void> SensorManager::createGrallocDirectChannel(const hardware::hidl_handle& buffer, uint64_t size, createGrallocDirectChannel_cb _hidl_cb) {
    // TODO implement
    return Void();
}

Return<void> SensorManager::createEventQueue(const sp<::android::frameworks::sensorservice::V1_0::IEventQueueCallback>& callback, createEventQueue_cb _hidl_cb) {
    // TODO implement
    sp<IEventQueue> queue = new EventQueue();
    _hidl_cb(queue, Result::OK);
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//ISensorManager* HIDL_FETCH_ISensorManager(const char* /* name */) {
    //return new SensorManager();
//}
//
}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android
