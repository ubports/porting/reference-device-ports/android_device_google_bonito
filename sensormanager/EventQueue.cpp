#include "EventQueue.h"

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {
namespace implementation {

// Methods from ::android::frameworks::sensorservice::V1_0::IEventQueue follow.
Return<::android::frameworks::sensorservice::V1_0::Result> EventQueue::enableSensor(int32_t sensorHandle, int32_t samplingPeriodUs, int64_t maxBatchReportLatencyUs) {
    // TODO implement
    return ::android::frameworks::sensorservice::V1_0::Result {};
}

Return<::android::frameworks::sensorservice::V1_0::Result> EventQueue::disableSensor(int32_t sensorHandle) {
    // TODO implement
    return ::android::frameworks::sensorservice::V1_0::Result {};
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IEventQueue* HIDL_FETCH_IEventQueue(const char* /* name */) {
    //return new EventQueue();
//}
//
}  // namespace implementation
}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android
